**Índice**  

 [[_TOC_]]  
  

# Entidad: Road

[Basado en la entidad Fiware Road](https://github.com/smart-data-models/dataModel.Transportation/tree/master/Road)

Descripción global: **Una descripción geográfica y contextual armonizada de una carretera. Actualmente esta entidad se actualiza cada 3 minutos e informa del estado de la congestión del trafico de un tramo.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.
- `description`: Una descripción de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon. 
- `roadClass`: La clasificación de esta carretera. Enum:'autopista, primaria, residencial, secundaria, de servicio, terciaria, troncal, sin clasificar': Valores permitidos: Los descritos por [OpenStreetMap](https://wiki.openstreetmap.org/wiki/Key:highway).
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.

##### Atributos descriptivos de la entidad (OPCIONALES)
- Ninguno

##### Atributos de la medición
- `fechaHora`: La fecha y la hora de esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `TimeInstant`: La fecha y la hora del envío del dato del agente IoT.
- `estadoForzado`: Indica el estado del tráfico. Si está en validación automática de recorrido o en fluido normalmente el valor será 0, si por el contrario es un valor asignado por el operador estará en valor 1. Pendiente de identificar el resto de valores posibles.

##### Atributos para el GIS / Representar gráficamente la entidad
- `idTramo`: Indica el identificador del tramo. Este atributo se usa posteriormente para poder relacionar dicho tramo con la BBDD del Gis.

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `owner`: Responsable de los datos de la entidad.
- `ownerEmail`: Email del responsable de los datos de la entidad.
- `respOCI`: Responsable del proyecto de la OCI.
- `respOCIEmail`: Email del responsable del proyecto de la OCI.
- `respOCITecnico`: Responsable Tecnico de la OCI
- `respOCITecnicoEmail`: Email del responsable tecnico de la OCI. 
- `responsible`: Responsable de los datos de la entidad.

##### Atributos innecesarios en futuras entidades
- Ninguno


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad             |
|----------------|------------------------|
| /trafico       | EstadoTrafico_1        |
| /trafico       | EstadoTrafico_2        |
| /trafico       | EstadoTrafico_3        |
| ....           | ....

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/EstadoTrafico_1' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /trafico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "1864",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/trafico",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "87c025ee-3a61-11ed-abc9-0a580a810238; cbnotif=4",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"EstadoTrafico_310\",\"type\":\"Road\",\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-22T10:30:08.208Z\",\"metadata\":{}},\"description\":{\"type\":\"Text\",\"value\":\"PASO INFERIOR GUILLÉN DE CASTRO - ÁNGEL GUIMERÁ\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-22T10:30:08.208Z\"}}},\"estadoForzado\":{\"type\":\"Number\",\"value\":5,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-22T10:30:08.208Z\"}}},\"fechaHora\":{\"type\":\"DateTime\",\"value\":\"2022-09-22T12:30:04.000Z\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-22T10:30:08.208Z\"}}},\"idTramo\":{\"type\":\"Number\",\"value\":310,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-22T10:30:08.208Z\"}}},\"location\":{\"type\":\"geo:point\",\"value\":\"39.46970272934208,-0.37636756896972656\",\"metadata\":{}},\"name\":{\"type\":\"Text\",\"value\":\"132\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-22T10:30:08.208Z\"}}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"owner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"ownerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCI\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCIEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnico\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnicoEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicio\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"respServicioEmail\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"responsible\":{\"type\":\"Number\",\"value\":\"XXXXX\",\"metadata\":{}},\"roadClass\":{\"type\":\"Text\",\"value\":\"living_street\",\"metadata\":{}},\"source\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "EstadoTrafico_310",
        "type": "Road",
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-09-22T10:30:08.208Z",
          "metadata": {}
        },
        "description": {
          "type": "Text",
          "value": "PASO INFERIOR GUILLÉN DE CASTRO - ÁNGEL GUIMERÁ",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-22T10:30:08.208Z"
            }
          }
        },
        "estadoForzado": {
          "type": "Number",
          "value": 5,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-22T10:30:08.208Z"
            }
          }
        },
        "fechaHora": {
          "type": "DateTime",
          "value": "2022-09-22T12:30:04.000Z",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-22T10:30:08.208Z"
            }
          }
        },
        "idTramo": {
          "type": "Number",
          "value": 310,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-22T10:30:08.208Z"
            }
          }
        },
        "location": {
          "type": "geo:point",
          "value": "39.46970272934208,-0.37636756896972656",
          "metadata": {}
        },
        "name": {
          "type": "Text",
          "value": "132",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-22T10:30:08.208Z"
            }
          }
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {}
        },
        "owner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "ownerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCI": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCIEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnico": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnicoEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicio": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "respServicioEmail": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "responsible": {
          "type": "Number",
          "value": "XXXXX",
          "metadata": {}
        },
        "roadClass": {
          "type": "Text",
          "value": "living_street",
          "metadata": {}
        },
        "source": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        }
      }
    ]
  }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================
- El GIS se actualiza mediante una ETL que accede al Postgres.
