Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/Road/spec.md) que se tiene. Estas entidades modelan una descripción geográfica y contextual armonizada de una carretera.
