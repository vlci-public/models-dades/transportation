**Índice**  

 [[_TOC_]]  
  

# Entidad: VehicleModel

[Basado en la entidad Fiware Vehicle](https://github.com/smart-data-models/dataModel.Transportation/tree/master/VehicleModel)

Descripción global: **Esta entidad modela un modelo de vehículo en particular, incluyendo todas las propiedades que son comunes a múltiples instancias de vehículos pertenecientes a dicho modelo.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad 
- `brandName`: Marca del vehículo
- `description`: Una descripción de este artículo
- `fuelType`: El tipo de combustible adecuado para el motor o los motores del vehículo. Enum:'autogás, biodiésel, etanol, gnc, diésel, eléctrico, gasolina, híbrido eléctrico/diésel, híbrido eléctrico/gasolina, hidrógeno, glp, gasolina, gasolina(sin plomo), gasolina(con plomo), otros'. 
- `manufacturerName`: Nombre del fabricante del vehículo  
- `modelName`: Nombre del modelo del vehículo  
- `name`: El nombre de este artículo.  
- `vehicleType`: Tipo de vehículo desde el punto de vista de sus características estructurales. Es diferente de la categoría del vehículo. Enum:'vehículo agrícola, cualquier vehículo, vehículo articulado, bicicleta, carro de basura, autobús, coche, caravana, coche o vehículo ligero, coche con caravana, coche con remolque, carro de limpieza, vehículo de construcción o mantenimiento, tracción a las cuatro ruedas, vehículo de gran altura, camión, minibús, ciclomotor, motocicleta, motocicleta con coche lateral, motocarro, máquina barredora, cisterna, vehículo de tres ruedas, remolque, tranvía, vehículo de dos ruedas, carro, furgoneta, vehículo sin convertidor catalítico, vehículo con caravana, vehículo con remolque, con matrícula par, con matrícula extra, otros". Los siguientes valores definidos por _VehicleTypeEnum_ y _VehicleTypeEnum2_, [DATEX 2 versión 2.3](http://d2docs.ndwcloud.nu/_static/umlmodel/v2.3/index.htm)  

##### Atributos de la medición
- Ninguno

##### Atributos para realizar cálculos / lógica de negocio
- Ninguno

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- Ninguno

##### Atributos innecesarios en futuras entidades
- Ninguno

## Lista de entidades que implementan este modelo
Las entidades cuenta con una frecuencia de actualización inferior a 1 minuto.

| Subservicio    | ID Entidad             |
|--------------------|------------------------|
| /medioambiente_emt | VehicleModel:195|
| /medioambiente_emt | VehicleModel:202|
| /medioambiente_emt | VehicleModel:203|
| /medioambiente_emt | VehicleModel:209|
| /medioambiente_emt | VehicleModel:210|


##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/VehicleModel:195' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /medioambiente_emt' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```


Atributos que van al GIS para su representación en una capa
===========================
- Ninguno. No existe suscripción al GIS para estas entidades
