Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/VehicleModel/spec.md) que se tiene. Contiene las entidades del proyecto C11, de la EMT. Estas entidades modelan un vehículo en particular, incluyendo todas las propiedades que son comunes a múltiples vehículos.