**Índice**  

 [[_TOC_]]  
  

# Entidad: Vehicle

[Basado en la entidad Fiware Vehicle](https://github.com/smart-data-models/dataModel.Transportation/tree/master/Vehicle)

Descripción global: **Esta entidad modela un modelo de vehículo en particular, incluyendo todas las propiedades que son comunes a múltiples instancias de vehículos pertenecientes a dicho modelo.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id`: Identificador único de la entidad
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `category`: Categoría de vehículo(s) desde un punto de vista externo. Es diferente del tipo de vehículo (coche, camión, etc.) representado por la propiedad `vehicleType`. Enum:'municipalServices, nonTracked, private, public, specialUsage, tracked'. Los vehículos rastreados son aquellos cuya posición es seguida permanentemente por un sistema remoto. O cualquier otro que necesite una aplicación Incorporan un receptor GPS junto con una conexión de red para actualizar periódicamente una posición reportada (ubicación, velocidad, rumbo...).
- `dataProvider`: Texto que describe quién envía los datos.
- `description`: Una descripción de este artículo
- `name`: El nombre de este artículo.
- `project`: proyecto al que pertenece esta entidad
- `vehicleType`: Tipo de vehículo desde el punto de vista de sus características estructurales. Es diferente de la categoría del vehículo. Enum:'vehículo agrícola, cualquier vehículo, vehículo articulado, bicicleta, carro de basura, autobús, coche, caravana, coche o vehículo ligero, coche con caravana, coche con remolque, carro de limpieza, vehículo de construcción o mantenimiento, tracción a las cuatro ruedas, vehículo de gran altura, camión, minibús, ciclomotor, motocicleta, motocicleta con coche lateral, motocarro, máquina barredora, cisterna, vehículo de tres ruedas, remolque, tranvía, vehículo de dos ruedas, carro, furgoneta, vehículo sin convertidor catalítico, vehículo con caravana, vehículo con remolque, con matrícula par, con matrícula extra, otros". Los siguientes valores definidos por _VehicleTypeEnum_ y _VehicleTypeEnum2_, [DATEX 2 versión 2.3](http://d2docs.ndwcloud.nu/_static/umlmodel/v2.3/index.htm) y ampliados para otros usos. En el proyecto de integración de emisoras de bomberos, este campo podrá tener únicamente los siguientes 4 valores: XXXXX

##### Atributos descriptivos de la entidad (OPCIONALES)

- `alternateName`: Nombre alternativo.
- `refVehicleModel`: Referencia del modelo del vehículo
- `vehiclePlateIdentifier`: Identificador o código que aparece en la placa de matrícula del vehículo y que se utiliza para su identificación oficial. El identificador de matrícula es numérico o alfanumérico y es único dentro de la región de la autoridad emisora. Referencias normativas: DATEXII `vehicleRegistrationPlateIdentifier` (identificador de la placa de matrícula del vehículo)
- `fleetVehicleId`: El identificador del vehículo en el contexto de la flota de vehículos a la que pertenece.
- `vehicleSpecialUsage`: Indica si el vehículo se ha utilizado para fines especiales, como el alquiler comercial, la autoescuela o como taxi. La legislación de muchos países exige que se revele esta información cuando se ofrece un coche a la venta. Enum:'ambulancia, bomberos, militar, policía, transporte escolar, taxi, gestión de la basura' .

##### Atributos de la medición

- `dateObserved`: Marca de tiempo de la última modificación de la entidad.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `speed`: Denota la magnitud del componente horizontal de la velocidad actual del vehículo y se especifica en kilómetros por hora. Si se proporciona, el valor del atributo velocidad debe ser un número real no negativo. PUEDE utilizarse `-1` si la velocidad se desconoce transitoriamente por alguna razón

##### Atributos de la medición (OPCIONALES)

- `heading`: Denota la dirección de desplazamiento del vehículo y se especifica en grados decimales, donde 0 <= `rumbo` < 360, contando en el sentido de las agujas del reloj respecto al norte verdadero. Si el vehículo está parado (es decir, el valor del atributo `velocidad` es `0`), el valor del atributo rumbo debe ser igual a `-1`.
- `serviceStatus`: Estado del vehículo (desde el punto de vista del servicio prestado, por lo que no podría aplicarse a los vehículos privados). `parked` : El vehículo está aparcado y no presta ningún servicio en este momento. `onRoute` : El vehículo está realizando una misión y en movimiento. `stopped` : El vehículo está realizando una misión y en parado. `unknown` : El vehículo está en un estado desconocido. Enum:'onRoute, stopped, parked, unknown'
- `measurementComments` : Específico de la integración de camiones de bomberos. Campo de texto libre que puede incorporar algún tipo de información asociada a la medición.

##### Atributos para realizar cálculos / lógica de negocio

- Ninguno

##### Atributos para el GIS / Representar gráficamente la entidad

- `showOnGIS`: Específico de la integración de camiones de bomberos. Booleano para ver si se visualiza o no la emisora (vehículo/persona) en el GIS municipal.

##### Atributos para la monitorización

- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.

## Atributos Multiidioma (Si se tienen, publicarlos)

- `alternateNameCas`: Nombre alternativo.
- `descriptionCas`: Descripción de esta entidad en castellano.
- `nameCas`: El nombre de este artículo en castellano.

##### Atributos innecesarios en futuras entidades

- Ninguno

## Lista de entidades que implementan este modelo

Existen 45 entidades con una frecuencia de actualización inferior a 1 minuto. Todas cumplen el patrón Vehicle:XXX, por ejemplo:

| Subservicio        | ID Entidad  |
| ------------------ | ----------- |
| /medioambiente_emt | Vehicle:173 |
| /medioambiente_emt | Vehicle:175 |
| /medioambiente_emt | Vehicle:206 |

Existen X entidades en el subservicio /bomberos que representan los camiones de bomberos. Todas cumplen el patrón Bomberos21400100011\*, por ejemplo:

| Subservicio | ID Entidad               |
| ----------- | ------------------------ |
| /bomberos   | Bomberos2140010001160100 |

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/Vehicle:173' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /medioambiente_emt' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

# Atributos que van al GIS para su representación en una capa

- Ninguno. No existe suscripción al GIS para estas entidades
