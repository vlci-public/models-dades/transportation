Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/Vehicle/spec.md) que se tiene. Contiene las entidades de los proyectos:
* Impulso C11, de la EMT. 
* Camiones de Bomberos. 

Estas entidades modelan un vehículo en particular, incluyendo todas las propiedades que son comunes a múltiples vehículos.