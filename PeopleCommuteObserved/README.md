Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/PeopleCommuteObserved/spec.md) que se tiene. Esta entidad modela una conteo de personas que viajan desde un punto origen a un punto destino. Esto se refiere a un desplazamiento de personas entre dos puntos con una periodicidad determinada.
