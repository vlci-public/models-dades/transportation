**Índice**  

 [[_TOC_]]  
  

# Entidad: PeopleCommuteObserved

Descripción global: **Conteo de personas/dispositivos únicos que viajan desde un punto origen a un punto destino. Esto se refiere a un desplazamiento de personas entre dos puntos con una periodicidad determinada.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id[string]`: Identificador único de la entidad
- `type[string]`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `name[string]`: El nombre de este artículo.
- `municipalityFrom[string]`: La ciudad/pueblo donde está situado el contador de origen del desplazamiento
- `municipalityTo[string]`: La ciudad/pueblo donde está situado el contador de destino del desplazamiento
- `locationFrom[geo:json]`: Referencia Geojson al elemento origen. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `locationTo[geo:json]`: Referencia Geojson al elemento destino. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `project[string]`: proyecto al que pertenece esta entidad

##### Atributos descriptivos de la entidad (OPCIONALES)

- `description[string]`: Descripción de este artículo
- `addressFrom[string]`: La dirección postal origen del desplazamiento
- `addressTo[string]`: La dirección postal destino del desplazamiento
- `refDevice[string]`: Una referencia al dispositivo que captó esta observación.

##### Atributos de la medición

- `dateObservedFrom[dateTime]`: Fecha y hora de inicio del periodo de observación.
- `dateObservedTo[dateTime]`: Fecha y hora de finalización del periodo de observación.
- `peopleCountWifi[number]`: Conteo de movimientos de personas que van desde un punto origen a un punto destino procedentes del wifi del dispositivo de la persona en el intervalo de tiempo indicado por el dateObservedFrom y dateObservedTo. Si una persona lleva varios dispositivos con el wifi activo, cuenta cada dispositivo diferente. Debido a limitaciones técnicas, no se tiene 100% de trazabilidad entre puntos origen-destino ya que el id no siempre es único. Hay modelos de móviles donde el id en origen es distinto al id de destino, con lo que no se pueden correlacionar y por tanto no forman parte del conteo.
- `peopleCountBluetooth[number]`: Conteo de movimientos de personas que van desde un punto origen a un punto destino procedentes del bluetooth del dispositivo de la persona en el intervalo de tiempo indicado por el dateObservedFrom y dateObservedTo. Si una persona lleva varios dispositivos con el bluetooth activo, cuenta cada dispositivo diferente. Debido a limitaciones técnicas, no se tiene 100% de trazabilidad entre puntos origen-destino ya que el id no siempre es único. Hay modelos de móviles donde el id en origen es distinto al id de destino, con lo que no se pueden correlacionar y por tanto no forman parte del conteo.
- `TimeInstant[dateTime]` : La fecha y la hora del envío del dato del agente IoT.

##### Atributos para realizar cálculos / lógica de negocio

N/A

##### Atributos para el GIS / Representar gráficamente la entidad

N/A

##### Atributos para la monitorización

- `operationalStatus[string]`: Posibles valores: ok, noData
- `maintenanceOwner[string]`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail[string]`: Email del responsable técnico de esta entidad
- `serviceOwner[string]`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail[string]`: Email de la persona del servicio municipal de contacto para esta entidad
- `inactive[boolean]`: Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.

## Lista de entidades que implementan este modelo (XX entidades)

|     Subservicio     | ID Entidad                      |
| ------------------- | ------------------------------- |
| /movilidad_personas | dipuval_VAACTMC0001-VAACTMC0101 |
| /movilidad_personas | ...                             |



##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/DipuVal_VAACTMC0001-VAACTMC0101' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /movilidad_personas' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

# Atributos que van al GIS para su representación en una capa

Al GIS sólo van las entidades que cumplen el siguiente patrón en su ID:

N/A

##### Atributos de la entidad context broker

- id
- type

##### Atributos descriptivos de la entidad

- name
- municipality
- location
- project

##### Atributos de la medición

- dateObservedFrom
- dateObservedTo
- peopleCountWifi
- peopleCountBluetooth

##### Atributos para el GIS / Representar gráficamente la entidad

N/A

##### Atributos para la monitorización

- operationalStatus
- maintenanceOwner
- maintenanceOwnerEmail
- serviceOwner
- serviceOwnerEmail
