Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/CrowdFlowObserved/spec.md) que se tiene. Estas entidades modelan el conteo de personas que pasan por un punto determinado durante un período de tiempo determinado.
