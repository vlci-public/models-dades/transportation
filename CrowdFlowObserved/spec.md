**Índice**  

 [[_TOC_]]  
  

# Entidad: CrowdFlowObserved

[Basado en la entidad Fiware CrowdFlowObserved](https://github.com/smart-data-models/dataModel.Transportation/tree/master/CrowdFlowObserved)

Descripción global: **Conteo de personas/dispositivos que pasan por un punto determinado durante un período de tiempo determinado.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id[string]`: Identificador único de la entidad
- `type[string]`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `name[string]`: El nombre de este artículo.
- `municipality[string]`: La ciudad/pueblo donde está situado el contador
- `location[geo:json]`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `project[string]`: proyecto al que pertenece esta entidad

##### Atributos descriptivos de la entidad (OPCIONALES)

- `description[string]`: Descripción de este artículo
- `address[string]`: La dirección postal
- `refDevice[string]`: Una referencia al dispositivo que captó esta observación.

##### Atributos de la medición

- `dateObservedFrom[dateTime]`: Fecha y hora de inicio del periodo de observación.
- `dateObservedTo[dateTime]`: Fecha y hora de finalización del periodo de observación.
- `peopleCountWifi[number]`: Conteo de detecciones de personas procedentes del wifi del dispositivo/s de la persona en el intervalo de tiempo indicado por el dateObservedFrom y dateObservedTo. Si una persona lleva varios dispositivos con el wifi activo, cuenta cada dispositivo diferente. Las mediciones del wifi tienen un mayor rango que las de bluetooth. Es decir van a contar dispositivos que pasen más lejos del detector wifi que los que pasen del detector bluetooth. Por contrapartida, el bluetooth es más preciso; es decir, si lo que quieres saber son los dispositivos que entran en un edificio, al ser el rango menor, no contará dispositivos que pasen a 50 metros de la puerta del edificio (cosa que sí haría el conteo wifi).
- `peopleCountBluetooth[number]`: Conteo de detecciones de personas procedentes del bluetooth del dispositivo/s de la persona en el intervalo de tiempo indicado por el dateObservedFrom y dateObservedTo. Si una persona lleva varios dispositivos con el bluetooth activo, cuenta cada dispositivo diferente. Nota específica para la integración con la Diputación de Valencia: actualmente (Jun-2024), la fiabilidad de estos conteos está en entredicho. Los sensores no acaban de medir correctamente. Es por tanto más fiable el conteo por wifi.
- `TimeInstant[dateTime]` : La fecha y la hora del envío del dato del agente IoT.


##### Atributos para realizar cálculos / lógica de negocio

N/A

##### Atributos para el GIS / Representar gráficamente la entidad

N/A

##### Atributos para la monitorización

- `operationalStatus[string]`: Posibles valores: ok, noData
- `maintenanceOwner[string]`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail[string]`: Email del responsable técnico de esta entidad
- `serviceOwner[string]`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail[string]`: Email de la persona del servicio municipal de contacto para esta entidad
- `inactive[boolean]`: Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.

## Lista de entidades que implementan este modelo (101 entidades)

|     Subservicio     | ID Entidad          |
| ------------------- | ------------------- |
| /movilidad_personas | DipuVal_VAACTMC0001 |
| /movilidad_personas | ...                 |
| /movilidad_personas | DipuVal_VAACTMC0101 |



##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/DipuVal_VAACTMC0001' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /movilidad_personas' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

# Atributos que van al GIS para su representación en una capa

Al GIS sólo van las entidades que cumplen el siguiente patrón en su ID:

N/A

##### Atributos de la entidad context broker

- id
- type

##### Atributos descriptivos de la entidad

- name
- municipality
- location
- project

##### Atributos de la medición

- dateObservedFrom
- dateObservedTo
- peopleCountWifi
- peopleCountBluetooth

##### Atributos para el GIS / Representar gráficamente la entidad

N/A

##### Atributos para la monitorización

- operationalStatus
- maintenanceOwner
- maintenanceOwnerEmail
- serviceOwner
- serviceOwnerEmail
