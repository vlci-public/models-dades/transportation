**Índice**  

 [[_TOC_]]  
  

# Entidad: EVChargingStation

[Basado en la entidad Fiware EVChargingStation](https://github.com/smart-data-models/dataModel.Transportation/tree/master/EVChargingStation)

Descripción global: **Estación de recarga de vehículos eléctricos**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id`: Identificador único de la entidad
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `address`: La dirección postal
- `allowedVehicleType`: Tipo(s) de vehículo permitido(s). El primer elemento de esta matriz DEBE ser el tipo de vehículo principal permitido. Los números de plaza libre de otros tipos de vehículos permitidos pueden notificarse bajo el atributo extraSpotNumber y a través de entidades específicas de tipo ParkingGroup. Los siguientes valores definidos por VehicleTypeEnum, DATEX 2 versión 2.3. Enum:'agriculturalVehicle, anyVehicle, bicycle, bus, caravan, carWithCaravan, carWithTrailer, constructionOrMaintenanceVehicle, lorry, moped, motorcycle, motorcycleWithSideCar, motorscooter, tanker, trailer, van'
- `capacity`: Número total de vehículos que pueden cargar simultáneamente, el cual puede ser inferior al número de conectores
- `chargeType`: Tipo(s) de carga realizado(s) por el aparcamiento. Valores permitidos: Algunos de los definidos por la enumeración DATEX II versión 2.3 _ ChargeTypeEnum_. Enum:'additionalIntervalPrice, annualPayment, firstIntervalPrice, flat, free, minimum, maximum, monthlyPayment, other, seasonTicket, temporaryPrice'. O cualquier otra aplicación específica
- `contactPoint`: URL específicamente preparada para el acceso al punto de recarga desde la App Valencia
- `dataProvider`: Nombre del operador del sistema establecido en la configuración del servicio
- `description`: Descripción de la zona
- `image`: Contiene la URL mediante la que se puede acceder a una imagen del punto de recarga
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `name`: El nombre de este artículo.
- `operator`: Nombre del operador del sistema establecido en la configuración del servicio
- `project`: Proyecto al que pertenece esta entidad.
- `refParkingSite`: Sitio de estacionamiento al que pertenece la plaza.
- `refSockets`: Referencias a los conectores del punto de recarga
- `socketNumber`: Número de conectores del punto de recarga
- `socketType`: Vector de tipos de conector [string] con los siguientes valores posibles: Caravan_Mains_Socket, CHAdeMO, CCS/SAE, Dual_CHAdeMO, Dual_J-1772, Dual_Mennekes, J-1772, Mennekes, Other, Tesla, Type2, Type3, Wall_Euro
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.

##### Atributos de la medición

- `availableCapacity`: Número de conectores actualmente disponibles
- `fraudStatus`: Campo cuyos valores posibles son Si o No y que indica la existencia de una situación de uso indebido de las plazas de aparcamiento adjuntas al punto de recarga, estando estas ocupadas sin haber una recarga activa en alguno de los conectores.
- `observationDateTime`: Instante de última observación (ISO 8601 con hora UTC, ejemplo:)
- `status`: Estado del punto de recarga en el sistema (valores posibles: almostEmpty, almostFull, empty, full, outOfService, withIncidence, working)

##### Atributos para la monitorización

- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `operationalStatus`: Posibles valores: ok, noData.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.

##### Atributos innecesarios en futuras entidades

- Ninguno

## Lista de entidades que implementan este modelo

| Subservicio                 | ID Entidad               |
| --------------------------- | ------------------------ |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PDR_04001 |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PDR_15001 |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PDR_06001 |
| ....                        | ....                     |

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/Impulso_ES_VLC_PDR_06001' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /recarga_vehiculo_electrico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

# Atributos que van al GIS para su representación en una capa

- id, location, address, capacity, refParkingSite, status, availableCapacity, operationalStatus, observationDateTime, project, contactPoint, fraudStatus
