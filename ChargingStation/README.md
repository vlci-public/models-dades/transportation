Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/ChargingStation/spec.md) que se tiene. Estas entidades modelan una estación de recarga de vehículos eléctricos.
