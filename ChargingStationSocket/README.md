Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/ChargingStationSocket/spec.md) que se tiene. Estas entidades modelan las mangueras de las estaciones de recarga de vehículos eléctricos.
