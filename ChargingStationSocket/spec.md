**Índice**  

 [[_TOC_]]  
  

# Entidad: EVChargingStationSocket

Fiware no tiene un modelo estándar para esta entidad. Este modelo por tanto es específico del proyecto VLCi.

Descripción global: **Conector o manguera de una estación de recarga de vehículos eléctricos**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id`: Identificador único de la entidad
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `contactPoint`: URL específicamente preparada para el acceso al punto de recarga desde la App Valencia
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `operator`: Nombre del operador del sistema establecido en la configuración del servicio
- `power`: Potencia nominal del conector
- `project`: Proyecto al que pertenece esta entidad.
- `refChargingStation`: Sitio de estacionamiento al que pertenece la plaza.
- `socketType`: Vector de tipos de conector [string] con los siguientes valores posibles: Caravan_Mains_Socket, CHAdeMO, CCS/SAE, Dual_CHAdeMO, Dual_J-1772, Dual_Mennekes, J-1772, Mennekes, Other, Tesla, Type2, Type3, Wall_Euro
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.

##### Atributos de la medición

- `observationDateTime`: Instante de última observación (ISO 8601 con hora UTC)
- `status`: Estado del punto de recarga en el sistema (valores posibles: almostEmpty, almostFull, empty, full, outOfService, withIncidence, working)

##### Atributos para el GIS

- `socketTypeGIS`: Vector de tipos de conector [string] con los siguientes valores posibles: Caravan_Mains_Socket, CHAdeMO, CCS/SAE, Dual_CHAdeMO, Dual_J-1772, Dual_Mennekes, J-1772, Mennekes, Other, Tesla, Type2, Type3, Wall_Euro.

##### Atributos para la monitorización

- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `operationalStatus`: Posibles valores: ok, noData.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.

##### Atributos innecesarios en futuras entidades

- Ninguno

## Lista de entidades que implementan este modelo

| Subservicio                 | ID Entidad                 |
| --------------------------- | -------------------------- |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PDR_04001_1 |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PDR_04001_2 |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PDR_04001_3 |
| ....                        | ....                       |

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/Impulso_ES_VLC_PDR_04001_3' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /recarga_vehiculo_electrico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

# Atributos que van al GIS para su representación en una capa

- id, status, socketTypeGIS, power, project, refChargingStation, operationalStatus, observationDateTime, location, contactPoint
