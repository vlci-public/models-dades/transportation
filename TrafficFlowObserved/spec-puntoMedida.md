Entidad: TrafficFlowObserved
===========================
  
Descripción global: **Una observación de las condiciones del flujo de tráfico en un lugar y momento determinados. Actualmente esta entidad se actualiza cada 3 minutos e informa del estado de la congestión del trafico de un punto de medida. Varios puntos de medida determinan un tramo**  

Utilizar los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon. 
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.
- `laneID`: Indica el número indicador del punto de medida.

##### Atributos descriptivos de la entidad (OPCIONALES)
- Ninguno

##### Atributos de la medición
- `dateObserved`:La fecha y la hora de esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `TimeInstant`: La fecha y la hora del envío del dato del agente IoT.
- `intensity`: Cantidad de vehículos detectados durante este periodo de observación extrapolado de Vehículos/Periodo a Vehiculos/Hora.
- `fiabilidad_intensidad`:Nivel de fiabilidad del valor de intensity.
- `tiempo_ocupacion`:Tiempo de ocupación en el punto de medida. El tiempo de ocupación es una medida porcentual, indica cuanto tiempo ha estado en el período de la medición un vehículo ocupando la espira.
- `fiabilidad_tiempo_ocupacion`:Nivel de fiabilidad del valor de tiempo_ocupacion.
- `typeData`: Indica de que tipo es el tramo. Posibles valores: TRAFICO, BICI.

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `owner`: Responsable de los datos de la entidad.
- `ownerEmail`: Email del responsable de los datos de la entidad.
- `respOCI`: Responsable del proyecto de la OCI.
- `respOCIEmail`: Email del responsable del proyecto de la OCI.
- `respOCITecnico`: Responsable Tecnico de la OCI
- `respOCITecnicoEmail`: Email del responsable tecnico de la OCI.

##### Atributos innecesarios en futuras entidades
- Ninguno


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad             |
|----------------|------------------------|
| /trafico       | puntoMedida_1537       |
| /trafico       | puntoMedida_2544       |
| /trafico       | puntoMedida_2048       |
| ....           | ....

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/puntoMedida_1537' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /trafico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "1965",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/trafico",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "4ce0e10a-3e3c-11ed-ad1e-0a580a810238; cbnotif=4",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"puntoMedida_2048_bici\",\"type\":\"TrafficFlowObserved\",\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T08:13:42.710Z\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T10:13:00.000Z\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T08:13:42.710Z\"}}},\"fiabilidad_intensidad\":{\"type\":\"Number\",\"value\":100,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T08:13:42.710Z\"}}},\"fiabilidad_tiempo_ocupacion\":{\"type\":\"Number\",\"value\":100,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T08:13:42.710Z\"}}},\"intensity\":{\"type\":\"Number\",\"value\":180,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T08:13:42.710Z\"}}},\"laneID\":{\"type\":\"Number\",\"value\":2048,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T08:13:42.710Z\"}}},\"location\":{\"type\":\"geo:point\",\"value\":\"39.46970272934208,-0.37636756896972656\",\"metadata\":{}},\"name\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"owner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"ownerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCI\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCIEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnico\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnicoEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicio\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"respServicioEmail\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"source\":{\"type\":\"Text\",\"value\":\"SQLServer\",\"metadata\":{}},\"tiempo_ocupacion\":{\"type\":\"Number\",\"value\":2,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T08:13:42.710Z\"}}},\"typeData\":{\"type\":\"Text\",\"value\":\"BICI\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T08:13:42.710Z\"}}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "puntoMedida_2048_bici",
        "type": "TrafficFlowObserved",
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-09-27T08:13:42.710Z",
          "metadata": {}
        },
        "dateObserved": {
          "type": "DateTime",
          "value": "2022-09-27T10:13:00.000Z",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T08:13:42.710Z"
            }
          }
        },
        "fiabilidad_intensidad": {
          "type": "Number",
          "value": 100,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T08:13:42.710Z"
            }
          }
        },
        "fiabilidad_tiempo_ocupacion": {
          "type": "Number",
          "value": 100,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T08:13:42.710Z"
            }
          }
        },
        "intensity": {
          "type": "Number",
          "value": 180,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T08:13:42.710Z"
            }
          }
        },
        "laneID": {
          "type": "Number",
          "value": 2048,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T08:13:42.710Z"
            }
          }
        },
        "location": {
          "type": "geo:point",
          "value": "39.46970272934208,-0.37636756896972656",
          "metadata": {}
        },
        "name": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {}
        },
        "owner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "ownerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCI": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCIEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnico": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnicoEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicio": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "respServicioEmail": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "source": {
          "type": "Text",
          "value": "SQLServer",
          "metadata": {}
        },
        "tiempo_ocupacion": {
          "type": "Number",
          "value": 2,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T08:13:42.710Z"
            }
          }
        },
        "typeData": {
          "type": "Text",
          "value": "BICI",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T08:13:42.710Z"
            }
          }
        }
      }
    ]
  }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================
- El GIS se actualiza mediante una ETL que accede al Postgres.
