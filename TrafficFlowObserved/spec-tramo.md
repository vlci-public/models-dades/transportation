Entidad: TrafficFlowObserved
===========================
  
Descripción global: **Una observación de las condiciones del flujo de tráfico en un lugar y momento determinados. Actualmente esta entidad se actualiza cada 3 minutos e informa del estado de la congestión del trafico de un tramo.**  

Utilizar los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon. 
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.

##### Atributos descriptivos de la entidad (OPCIONALES)
- Ninguno

##### Atributos de la medición
- `dateObserved`:La fecha y la hora de esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `TimeInstant`: La fecha y la hora del envío del dato del agente IoT.
- `intensity`: Cantidad de vehículos detectados durante este periodo de observación extrapolado de Vehículos/Periodo a Vehiculos/Hora. 
- `typeDate`: Indica de que tipo es el tramo. Posibles valores: TRAFICO, BICI.

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `owner`: Responsable de los datos de la entidad.
- `ownerEmail`: Email del responsable de los datos de la entidad.
- `respOCI`: Responsable del proyecto de la OCI.
- `respOCIEmail`: Email del responsable del proyecto de la OCI.
- `respOCITecnico`: Responsable Tecnico de la OCI
- `respOCITecnicoEmail`: Email del responsable tecnico de la OCI.

##### Atributos innecesarios en futuras entidades
- Ninguno


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad             |
|----------------|------------------------|
| /trafico       | tramo_A1               |
| /trafico       | tramo_A10              |
| /trafico       | tramo_A102             |
| ....           | ....

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/tramo_A1' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /trafico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "1746",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/trafico",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "edccd1d4-3e35-11ed-99d2-0a580a810238; cbnotif=4",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"tramo_A1\",\"type\":\"TrafficFlowObserved\",\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T07:28:06.217Z\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T09:28:01.000Z\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T07:28:06.217Z\"}}},\"fiabilidad_intensidad\":{\"type\":\"Number\",\"value\":\"\",\"metadata\":{}},\"fiabilidad_tiempo_ocupacion\":{\"type\":\"Number\",\"value\":\"\",\"metadata\":{}},\"intensity\":{\"type\":\"Number\",\"value\":-1,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T07:28:06.217Z\"}}},\"laneID\":{\"type\":\"Number\",\"value\":\"\",\"metadata\":{}},\"location\":{\"type\":\"geo:point\",\"value\":\"39.46970272934208,-0.37636756896972656\",\"metadata\":{}},\"name\":{\"type\":\"Text\",\"value\":\"A1\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T07:28:06.217Z\"}}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"owner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"ownerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCI\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCIEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnico\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnicoEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicio\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"respServicioEmail\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"source\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"tiempo_ocupacion\":{\"type\":\"Number\",\"value\":\"\",\"metadata\":{}},\"typeData\":{\"type\":\"Text\",\"value\":\"BICI\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-27T07:28:06.217Z\"}}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "tramo_A1",
        "type": "TrafficFlowObserved",
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-09-27T07:28:06.217Z",
          "metadata": {}
        },
        "dateObserved": {
          "type": "DateTime",
          "value": "2022-09-27T09:28:01.000Z",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T07:28:06.217Z"
            }
          }
        },
        "fiabilidad_intensidad": {
          "type": "Number",
          "value": "",
          "metadata": {}
        },
        "fiabilidad_tiempo_ocupacion": {
          "type": "Number",
          "value": "",
          "metadata": {}
        },
        "intensity": {
          "type": "Number",
          "value": 15,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T07:28:06.217Z"
            }
          }
        },
        "laneID": {
          "type": "Number",
          "value": "",
          "metadata": {}
        },
        "location": {
          "type": "geo:point",
          "value": "39.46970272934208,-0.37636756896972656",
          "metadata": {}
        },
        "name": {
          "type": "Text",
          "value": "A1",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T07:28:06.217Z"
            }
          }
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {}
        },
        "owner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "ownerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCI": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCIEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnico": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnicoEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicio": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "respServicioEmail": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "source": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "tiempo_ocupacion": {
          "type": "Number",
          "value": "",
          "metadata": {}
        },
        "typeData": {
          "type": "Text",
          "value": "BICI",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-27T07:28:06.217Z"
            }
          }
        }
      }
    ]
  }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================
- El GIS se actualiza mediante una ETL que accede al Postgres.
