# Entidad: TrafficFlowObserved

Descripción global: \*_Una observación de las condiciones del flujo de tráfico en un lugar y momento determinados. Actualmente esta entidad se actualiza cada 5 minutos e informa de la intensidad de bicis que han pasado en la última hora y el número de bicis que han pasado en los últimos 5 minutos para un tramo determinado_

Utilizar los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id`: Identificador único de la entidad
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `description`: Nombre del tramo en valenciano al que hace referencia.
- `descriptionCas`: Nombre del tramo en castellano al que hace referencia.
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.
- `name`: IdTA del tramo. Valor numérico.
- `vehicleType`: Indica de que tipo es el vehículo.
- `locationForGISVisualization`: Referencia Geojson al elemento. Atributo de tipo Point, utilizado en geoportal.
##### Atributos descriptivos de la entidad (OPCIONALES)

- Ninguno

##### Atributos de la medición

- `dateObserved`:La fecha y la hora de esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `dateObservedTo`:La fecha y la hora en la que se inició esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `dateObservedFrom`:La fecha y la hora en la que finaliza esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `intensity`: Valor de la intensidad de bicis
- `updateAt`:La fecha y la hora en la que se envía la medición.
- `lowerthresholdrt`:Umbral de intensidad de bicis baja.
- `middlethresholdrt`:Umbral de intensidad de bicis media.
- `upperThresholdrt`:Umbral de intensidad de bicis alta.

##### Atributos para el GIS / Representar gráficamente la entidad

- Ninguno

##### Atributos para la monitorización

- `operationalStatus`: Posibles valores: ok, noData
- `project`: Proyecto en el que se utiliza esta entidad.
- `processName`: Proceso desde el que se actualiza la entidad.
- `organization`: Organización
- `maintenanceOwner`: Responsable del mantenimiento de la entidad
- `maintenanceOwnerEmail`: Email del responsable del mantenimiento de la entidad

##### Atributos innecesarios en futuras entidades

- Ninguno

## Lista de entidades que implementan este modelo

| Subservicio | ID Entidad                   |
| ----------- | ---------------------------- |
| /trafico    | intensidad_bicis_rt_fXX      |
| /trafico    | intensidad_bicis_daily_fXX   |
| /trafico    | intensidad_bicis_monthly_fXX |
| /trafico    | intensidad_bicis_annual_fXX  |

##### Ejemplo de entidad

curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/intensidad_bicis_rt_f26' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /trafico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'

```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "620",
    "user-agent": "orion/3.8.1 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/trafico",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "08c458af-436e-4e39-9930-da6c0370a520; cbnotif=1",
    "ngsiv2-attrsformat": "normalized"
  },
"data":[{"id":"intensidad_bicis_rt_f26","type":"TrafficFlowObserved","description":{"type":"Text","value":"PIO BAROJA","metadata":{}},"descriptionCas":{"type":"Text","value":"PIO BAROJA","metadata":{}},"dateObserved":{"type":"Text","value":"2023-02-20T14:97:00.000z","metadata":{}},"intensity":{"type":"Number","value":19,"metadata":{}},"hourlyIntensity":{"type":"Number","value":340,"metadata":{}},"location":{"type":"geo:point","value":"0,0","metadata":{}},"name":{"type":"Text","value":"652","metadata":{}},"operationalStatus":{"type":"Text","value":"ok","metadata":{}}}]}

  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data":[{\"id\":\"intensidad_bicis_rt_f26\",\"type\":\"TrafficFlowObserved\",\"description\":{\"type\":\"Text\",\"value\":\"PIO BAROJA\",\"metadata\":{}},\"descriptionCas\":{\"type\":\"Text\",\"value\":\"PIO BAROJA\",\"metadata\":{}},\"dateObserved\":{\"type\":\"Text\",\"value\":\"2023-02-20T14:97:00.000z\",\"metadata\":{}},\"intensity\":{\"type\":\"Number\",\"value\":19,\"metadata\":{}},\"hourlyIntensity\":{\"type\":\"Number\",\"value\":340,\"metadata\":{}},\"location\":{\"type\":\"geo:point\",\"value\":\"0,0\",\"metadata\":{}},\"name\":{\"type\":\"Text\",\"value\":\"652\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
        {
            "id": "intensidad_bicis_rt_f26",
            "type": "TrafficFlowObserved",
            "description": {
                "type": "Text",
                "value": "PIO BAROJA",
                "metadata": {}
            },
            "descriptionCas": {
                "type": "Text",
                "value": "PIO BAROJA",
                "metadata": {}
            },
            "dateObserved": {
                "type": "Text",
                "value": "2023-02-20T14:97:00.000z",
                "metadata": {}
            },
            "intensity": {
                "type": "Number",
                "value": 19,
                "metadata": {}
            },
            "hourlyIntensity": {
                "type": "Number",
                "value": 340,
                "metadata": {}
            },
            "location": {
                "type": "geo:point",
                "value": "0,0",
                "metadata": {}
            },
            "name": {
                "type": "Text",
                "value": "652",
                "metadata": {}
            },
            "operationalStatus": {
                "type": "Text",
                "value": "ok",
                "metadata": {}
            }
        }
    ]
  }
}
</details>

```
