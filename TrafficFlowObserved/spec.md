**Índice**  

 [[_TOC_]]  
  

# Entidad: TrafficFlowObserved

[Basado en la entidad Fiware TrafficFlowObserved](https://github.com/smart-data-models/dataModel.Transportation/tree/master/TrafficFlowObserved)

Descripción global: **Una observación de las condiciones del flujo de tráfico en un lugar y momento determinados.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `description`: Nombre del tramo al que hace referencia.(En Valenciano)
- `descriptionCas`: Nombre del tramo al que hace referencia en castellano.
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.
- `name`: IdTA del tramo. Valor numérico.
- `laneId`: IdTA del tramo. Valor numérico.
- `vehicleType`: Indica de que tipo es el vehículo.

##### Atributos descriptivos de la entidad (OPCIONALES)
- `idPM`: Indica el número indicador del punto de medida.
- `laneDirection`: Sentido habitual de la marcha en el carril al que se refiere esta observación.
- `typeData`: Indica de que tipo es el tramo. Posibles valores: TRAFICO, BICI.


##### Atributos de la medición 
- `dateObserved`:La fecha y la hora de esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `dateObservedTo`:La fecha y la hora en la que se inició esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `dateObservedFrom`:La fecha y la hora en la que finaliza esta observación en formato ISO8601, convertido al timezone Europe/Madrid.
- `intensity`:  Cantidad de vehículos detectados durante este periodo de observación
- `updateAt`:La fecha y la hora en la que se envía la medición.

##### Atributos de la medición (OPCIONALES)
- `intensityReliability`:Fiabilidad de la intensidad.
- `occupancy`:Fracción del tiempo de observación en el que un vehículo ha estado ocupando el carril observado.
- `occupancyReliability`:Fiabilidad del tiempo de ocupación.


##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData.Funcionamiento normal: en cada envío establecer ese atributo a ok. Internamente, la plataforma de ciudad establecerá el valor noData en caso de pasar un X tiempo sin recibir datos del indicador.
- `project`: Proyecto en el que se utiliza esta entidad.
- `processName`: Proceso desde el que se actualiza la entidad.
- `organization`: Organización
- `maintenanceOwner`: Responsable del mantenimiento de la entidad
- `maintenanceOwnerEmail`: Email del responsable del mantenimiento de la entidad

##### Atributos para la monitorización (OPCIONALES)
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.


##### Atributos innecesarios en futuras entidades
- Ninguno


## Lista de entidades que implementan este modelo y contaminantes disponibles

| Subservicio    | ID Entidad                   |
|----------------|------------------------------|
| /Trafico       | puntoMedidaDiaria-1-tra      |
| /Trafico       | puntoMedidaDiaria-2-tra      |
| /Trafico       | puntoMedidaDiaria-3-tra      |
| ...            | ...                          |
| /Trafico       | puntoMedidaDiaria-1143-bici  |
| /Trafico       | puntoMedidaDiaria-1144-bici  |
| /Trafico       | puntoMedidaDiaria-1145-bici  |
| ...            | ...                          |


##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/puntoMedidaDiaria_1_tra' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /Trafico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)
<details><summary>Click para ver la respuesta</summary>
PENDIENTE
</details>

Atributos que van al GIS para su representación en una capa
===========================
- Ninguno. No existe suscripción al GIS para estas entidades
