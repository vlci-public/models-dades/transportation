Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/TrafficFlowObserved/spec.md) que se tiene. Estas entidades modelan una observación de las condiciones del flujo de tráfico en un lugar y tiempo determinados.

Especificación: Intensidad de tramos Real Time 
===
Entidades con datos calculados por parte de una ETL, con periodicidad Real Time [Especificación Intensidad de tramos RT](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/TrafficFlowObserved/spec-tramo.md)

Especificación: Intensidad de puntos de medida Real Time 
===
Entidades con datos calculados por parte de una ETL, con periodicidad Real Time [Especificación Intensidad de puntos de medida RT](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/TrafficFlowObserved/spec-puntoMedida.md)

Especificación: Intensidad de bicis 
===
Entidades con datos calculados por parte de una ETL, con diferentes periodicidades [Especificación Intensidad de bicis](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/TrafficFlowObserved/spec-intensidadBicis.md)
