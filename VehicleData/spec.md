**Índice**  

 [[_TOC_]]  
  

# Entidad: VehicleData

Fiware no tiene un modelo estándar para esta entidad. Este modelo por tanto es específico del proyecto VLCi.

Descripción global: **Esta entidad modela los datos de un vehículo en particular.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `project`: Proyecto al que pertenece esta entidad
- `refDevice`: Referencia al dispositivo

##### Atributos de la medición
- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601 UTC  
- `TimeInstant`: La fecha y la hora del envío del dato del agente IoT
- `engCoolantTemp`: Temperatura del refrigerante del motor 
- `engOilTemp1`: Temperatura del aceite del motor
- `heading`: 
- `rampWheelChairLiftPos`: 
- `rpm`: Revoluciones por minuto
- `speed`: Velocidad del vehículo 

##### Atributos para realizar cálculos / lógica de negocio
- Ninguno

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos innecesarios en futuras entidades
- Ninguno

## Lista de entidades que implementan este modelo
Actualmente existen 58 entidades con una frecuencia de actualización inferior a 1 minuto. Todas cumplen el patrón VehicleData:XXX, por ejemplo:

| Subservicio    | ID Entidad             |
|--------------------|------------------------|
| /medioambiente_emt | VehicleData:8MGV2DMEAO|
| /medioambiente_emt | VehicleData:WA4FA7H7M9|


##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/VehicleData:WA4FA7H7M9' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /medioambiente_emt' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```


Atributos que van al GIS para su representación en una capa
===========================
- Ninguno. No existe suscripción al GIS para estas entidades
