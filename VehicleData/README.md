Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/VehicleData/spec.md) que se tiene. Contiene las entidades del proyecto C11, de la EMT. Son entidades que reciben datos en modo realtime, directamente desde el agente. 