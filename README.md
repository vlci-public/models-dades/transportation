# Transportation

Modelos de datos del proyecto VLCi basados en los modelos de datos Fiware: https://github.com/smart-data-models/dataModel.Transportation

## Listado de modelos de datos

Los siguientes tipos de entidad están disponibles:

- [EVChargingStation](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/ChargingStation/README.md). Esta entidad modela una estación de carga de vehículo eléctrico.

- [EVChargingStationSocket](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/ChargingStationSocket/README.md).  Esta entidad modela una manguera de una estación de carga de vehículo eléctrico.

- [Vehicle](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/Vehicle/README.md). Esta entidad modela un vehículo en particular.

- [VehicleModel](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/VehicleModel/README.md). Esta entidad modela un modelo de vehículo en particular, incluyendo todas las propiedades que son comunes a múltiples instancias de vehículos pertenecientes a dicho modelo.

- [VehicleData](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/VehicleData/README.md). Esta entidad modela un vehículo en particular.

- [TrafficFlowObserved](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/TrafficFlowObserved/README.md). Una observación de las condiciones de fluidez del tráfico en un lugar y momento determinados.

- [Road](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/Road/README.md). Esta entidad contiene una descripción geográfica y contextual armonizada de una carretera.

- [CrowdFlowObserved](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/CrowdFlowObserved/README.md). Esta entidad modela una conteo de personas que pasan por un punto determinado.

- [PeopleCommuteObserved](https://gitlab.com/vlci-public/models-dades/transportation/-/blob/main/PeopleCommuteObserved/README.md). Esta entidad modela una conteo de personas que viajan desde un punto origen a un punto destino. Esto se refiere a un desplazamiento de personas entre dos puntos con una periodicidad determinada.
